# help_functions_hmm.py 
# daaboul@fzi.de

import numpy as np

class HMM_sol:
    # First we start with the initialization.
    def __init__(self,S,V,A,B,Pi):
        #constructor
        # State vector S
        # Output vector V
        # Transition_probs A[i, j] is the probability of transitioning to state i from state j
        # Emission_probs B[i, j] is the probability of emitting emission j while in state i 
        # Initial_dist Pi[i] is the intial probability of the state i
        self.S = S
        self.V = V
        self.A = A
        self.B = B
        self.Pi = Pi
    
    @property
    def num_states(self):
        return self.A.shape[0]
      
    def Prob_OutputSequence_Given_StateSequence(self,Q,O):
        # Parameters:
        # Q = StateSequence
        # O = Output Sequence
        # The lenght of the time sequence
        T = len(Q)
    
        # Initial probability of the first state q_0 in the sequence times the 
        # probability to observe o_0 in q_0
        Gamma  = self.Pi[Q[0]]*self.B[Q[0],O[0]]
    
        # Calculate Gamma (ToDo)
        for t in range(1, T):
            Gamma *= self.A[Q[t-1],Q[t]]*self.B[O[t],Q[t]]

        return Gamma

    def forward(self,O):
        # O = Output Sequence
        N = self.num_states
        T = len(O)

        # Initalization
        alpha = np.zeros((T, N))
        alpha[0] = self.Pi* self.B[O[0]]

        # Induction (TODO)
        for t in range(1, T):
            alpha[t] = alpha[t-1].dot(self.A) * self.B[O[t]]

        # using the forward part of the forward-backward algorithm
        P = alpha[-1].sum()
    
        return P
  
  
    def backward(self,O):
        N = self.num_states
        T = len(O)

        # Initalization
        beta = np.zeros((N,T))
        beta[:,-1:] = 1

        # Induction
        for t in reversed(range(T-1)):
            for n in range(N):
                beta[n,t] = np.sum(beta[:,t+1] * A[n,:] * B[:, obs_seq[t+1]])

        return beta

        
    def viterbi(self, O):
        N = self.num_states
        T = len(O)

        delta = np.zeros((T, N))
        psi = np.zeros((T, N))
        delta[0] = self.Pi*self.B[O[0]]
        for t in range(1, T):
            for j in range(N):
                delta[t,j] = np.max(delta[t-1]*self.A[:,j]) * self.B[O[t],j]
                psi[t,j] = np.argmax(delta[t-1]*self.A[:,j])

        # backtrack'
        states = np.zeros(T, dtype=np.int32)
        states[T-1] = np.argmax(delta[T-1])
        for t in range(T-2, -1, -1):
            states[t] = psi[t+1, states[t+1]]
        return states


def test_Hmm_matrices():
    # The state vector:
    S = np.array([0, 1])
    # The Output vector:
    V = np.array([0, 1, 2])
    #  transition Matrix 
    A = np.array([[0.7, 0.3],[ 0.4, 0.6]])
    # emission 
    B = np.array([[0.5, 0.4 , 0.1],
                  [0.1, 0.3,0.6]]).T
    
    # the initial probability 
    Pi = np.array([0.6,0.4])
    
    return S,V,A, B, Pi
  
def print_state():
    return ['Healthy', 'Fever']
  

def print_observation():
    return ['normal', 'cold', 'dizzy']
    
    
def test_Hmm_state_seq():
    # the state sequence
    Q = np.array([0,1,1,0,1])
    
    return Q


def test_Hmm_observation_seq():
    # the state sequence
    Q = np.array([0,1,1,0,1])
    
    return Q
  
def task3_answer():
    # V state vector (TO DO)
    S = np.array([0,1,2,3,4])

    # V observation vector (TO DO)
    V = np.array([0,1])

    # A matrix is 6*6 dim matrix 
    A = np.array([[0.,0.6,0.4,0.,0.,0.],
                 [0.,0.,0.,0.9,0.1,0.],
                 [0.,0.,0.,0.,0.2,0.8],
                 [0.,0.,0.,0.,0. , 1.],
                 [0.,0.,0.,0.,0. , 1.],
                 [1.,0.,0.,0.,0.,0.]])

    # B matrix is 6*2 dim matrix
    B = np.array([[0.8, 0.2],
                  [0.8, 0.2],
                  [ 0.5,  0.5],
                  [ 0.7,  0.3],
                  [ 0.2,  0.8],
                  [ 0.3,  0.7]]).T

    # Pi matrix is 1*6 dim matrix
    Pi = np.array([1.,0.,0.,0.,0.,0.])
    
    return S,V,A,B,Pi
