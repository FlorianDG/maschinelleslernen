from sklearn.naive_bayes import GaussianNB
from sklearn.pipeline import make_pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB

def task1_answer(X,y):
    model = GaussianNB()
    model.fit(X, y)
    return model


def task2_answer(train):
    model = make_pipeline(TfidfVectorizer(), MultinomialNB())
    model.fit(train.data, train.target)
    return model
     