{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A simple Neural Network for MNIST data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Necessary imports**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "import skimage.measure\n",
    "%matplotlib inline\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "from ipywidgets import interact, interactive, fixed, interact_manual, IntSlider, IntText, Play, jslink"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Load and visualize MNIST dataset**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the first part we want to implement a small neural network which detects handwritten digits. To this end we use the MNIST dataset which is extensively used as academic benchmark for machine learning algorithms. As it is a standard dataset, we can load it from Tensorflow's high-level API Keras. The following cell introduces the dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "type of training input: <class 'numpy.ndarray'>\n",
      "type of training labels: <class 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "#load MNIST data\n",
    "mnist = tf.keras.datasets.mnist\n",
    "(x_train_large, y_train),(x_test_large, y_test) = mnist.load_data()\n",
    "#scale int8 to the interval (0,1)\n",
    "x_train_large, x_test_large = x_train_large / 255.0, x_test_large / 255.0\n",
    "\n",
    "#reduce size of images\n",
    "reduce_fac = 2\n",
    "image_size = int(x_train_large.shape[-1]/reduce_fac)\n",
    "x_train = np.zeros([len(x_train_large), image_size, image_size], dtype=np.float64)\n",
    "x_test = np.zeros([len(x_test_large), image_size, image_size], dtype=np.float64)\n",
    "for i in range(len(x_train_large)):\n",
    "    x_train[i] = skimage.measure.block_reduce(x_train_large[i], (reduce_fac,reduce_fac), np.max)\n",
    "for i in range(len(x_test_large)):\n",
    "    x_test[i] = skimage.measure.block_reduce(x_test_large[i], (reduce_fac,reduce_fac), np.max)\n",
    "\n",
    "\n",
    "#find out type of x_train and y_train\n",
    "print(\"type of training input: \" + str(type(x_train)))\n",
    "print(\"type of training labels: \" + str(type(y_train)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#find out shapes of x_train and y_train\n",
    "print(#TODO)\n",
    "print(#TODO)\n",
    "#print first 20 labels in y_train\n",
    "print(#TODO)\n",
    "\n",
    "\n",
    "#plot first 20 images in x_train\n",
    "fig = plt.figure(figsize=(12, 12))\n",
    "plt.title(\"first 20 images in x_train\")\n",
    "plt.axis('off')\n",
    "for i in range(20):\n",
    "    sub = fig.add_subplot(4, 5, i + 1)\n",
    "    sub.imshow(x_train[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Notation**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " For the implementation of the neural network the following notation will be helpful. Nodes (neurons) are specified by a the specification of a layer and a numbering within that layer. Consequently the activation of the $j$-th node of layer $l$ is denoted by $x^{(l)}_j$. Similarlly the bias of this node is written as $b^{(l)}_j$. Let $x^{(l)}$ and $b^{(l)}$ be the column vectors containing all the activations and biases in layer $l$, respectively. Furthermore, except for the input layer, each layer $l$ is assigned one matrix $W^{(l)}$. The element $w^{(l)}_{jk}$ of the matrix $W^{(l)}$ represents the edge from node $k$ of layer $l-1$ to node $j$ of layer $l$ (siehe Abbildung 1). As discussed in the lecture, we will use the so-called bias trick, in order to have a unified notation for biases and weights in one matrix. This means that the bias vector is just the $0$-th column of $W^{(l)}$, i.e. $b^{(l)}=w^{(l)}_{j0}$, and the activation of the added $0$-th node is one, i.e. $x^{(l)}_0=1$. Finally we denote the activation function in layer $l$ by $f^{(l)}$ as it is assumed to be the same within one layer. It maps the accumulated input $z^{(l)} = W^{(l)}\\cdot x^{(l-1)}$ of the layer to its activation $x^{(l)}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"nn_layer.png\" style=\"width:50%;\">\n",
    "<caption><center> <u> Abbildung 1 </u>: Weight matrix for one layer $l$ of a feedforward network. Note that the bias trick is not included in the picture.<br> </center></caption>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Network architecture**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " We will use a simple fully-connected feedforward network with three layers, one input layer one hidden layer and one output layer. The size of the input layer is determined by the image size (number of pixels). We choose the hidden layer to have $30$ and the output layer as many nodes as there are possible labels. The output is (up to normalization) interpreted as probability distribution over the ten digits: The output of the $i$-th node represents the probablity that the input is an image of a digit $i\\in\\{0,9\\}$. E.g., the output vector $\\left(1,0,0,0,0,0,0,0,0,0\\right)$ corresponds to a $100\\%$-certainty that the image shows a zero. This kind of class-encoding is often called one-hot-encoding. In order to guarantee that the activations in the last layer are normalized and lie in the interval $[0,1]$, we use the softmax activation function\n",
    "<br><br>\\begin{align}\n",
    "f_i^{(2)}\\left(\\{z_k\\}\\right)=\\frac{\\exp{z_i}}{\\sum_k \\exp{z_k}}.\n",
    "\\end{align}<br>\n",
    "For $f^{(1)}$ we will use the logistic function (often called sigmoid function)\n",
    "<br><br>\\begin{align}\n",
    "f_i^{(1)}\\left(\\{z_k\\}\\right)=\\frac{1}{1+\\exp{z_i}}.\n",
    "\\end{align}<br><br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [],
   "source": [
    "#set sizes of layers\n",
    "input_size = #TODO\n",
    "hidden_size = #TODO\n",
    "output_size = #TODO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 149,
   "metadata": {},
   "outputs": [],
   "source": [
    "#define first layer activation function: logistic function\n",
    "def f_1(x):\n",
    "#TODO\n",
    "#TODO\n",
    "    \n",
    "#vectorization of f_1\n",
    "v_f_1 = np.vectorize(f_1)\n",
    "\n",
    "#define second layer activation function: softmax function\n",
    "def f_2(x, delta=10**(-8)):\n",
    "    shift_x = x - np.max(x)\n",
    "    y = np.exp(shift_x)/(np.sum(np.exp(shift_x), axis=0) + delta)\n",
    "    return y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Initialization of weights**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We generate small, random, initial weight matrices by sampling from a standard normal distribution $\\cal{N}(0,\\sigma)$ with $\\sigma=0.001$. The matrices need to have dimensions which are consistent with the network architecture and the bias trick. Hint: Use the command np.random.normal($\\mu$, $\\sigma$, \\[dimension_1, dimension_2\\])."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 150,
   "metadata": {},
   "outputs": [],
   "source": [
    "#define function that returns initial weight matrices (including biases)\n",
    "def init_weights(input_size, hidden_size, output_size, sigma=0.0001):\n",
    "    \n",
    "    #generate first layer weight matrix\n",
    "    #TODO\n",
    "    \n",
    "    #generate second layer weight matrix\n",
    "    #TODO\n",
    "    \n",
    "    return W_1, W_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Forward Propagation (Prediction)**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Applying the bias trick, the forward propagation across the layer $l$, then reads\n",
    "<br><br>\n",
    "\\begin{align}\n",
    "x^{(l)} = f^{(l)}\\left(W^{(l)}\\cdot x^{(l-1)}\\right).\n",
    "\\end{align}<br>\n",
    "In our present example we define input, hidden and output layer to be layer $0$, $1$ and $2$, respectively. The two weight matrices $W^{(1)}$ (shape: $30\\times(196+1)$) and $W^{(2)}$ (shape: $10\\times(30+1)$) together with the activation functions $f^{(1)}$ and $f^{(2)}$ completely define the forward propagation through the network of an input $x^{(0)}$ to an output $y_{pred}$:\n",
    "<br><br>\\begin{align}\n",
    "  y_{pred} = x^{(2)} = f^{(2)}\\left(W^{(2)}\\cdot f^{(1)}\\left(W^{(1)}\\cdot x^{(0)}\\right)\\right).\n",
    "\\end{align}<br>\n",
    "Since we do not only have one training example, it would be interesting to process $n$ several training examples to the same time in so-called batches. This can be done via extension of the vector $x^{(0)}$ to a matrix $X^{(0)}$, where each column of $X^{(0)}$ represents one training example. All the other involved vectors are casted to matrices by the added batch size dimension, too. E.g. $Y_{pred}$ has shape $10\\times n$.\n",
    "<br><br>\\begin{align}\n",
    "  Y_{pred} = X^{(2)} = f^{(2)}\\left(W^{(2)}\\cdot f^{(1)}\\left(W^{(1)}\\cdot X^{(0)}\\right)\\right).\n",
    "\\end{align}<br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 151,
   "metadata": {},
   "outputs": [],
   "source": [
    "#function that returns the activation of all layers given input and weight matrices\n",
    "def predict(x, W_1, W_2):\n",
    "    \n",
    "    #cast array of 2-d images into a matrix of shape batch_size*input_size\n",
    "    batch_size = x.shape[0]\n",
    "    X_0 = x.reshape(batch_size, input_size).T\n",
    "    #add bias input \n",
    "    X_0 = np.insert(X_0, 0, np.ones(batch_size), axis=0)\n",
    "    #matrix multiplication of first layer\n",
    "    \n",
    "    Z_1 = #TODO\n",
    "    #apply activation function of first layer\n",
    "    X_1 = #TODO\n",
    "    #add bias input\n",
    "    X_1 = np.insert(Z_1, 0, np.ones(batch_size), axis=0)\n",
    "    \n",
    "    #matrix multiplication of second layer\n",
    "    Z_2 = #TODO\n",
    "    #apply activation function of second layer\n",
    "    Y_pred = #TODO\n",
    "    \n",
    "    return X_0, X_1, Y_pred"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Regularized cross entropy loss and gradient**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The network learns from training samples based on a loss function. This is usually some kind of distance measure between the forward propageted training batch (i.e., the network output $Y_{pred}$) and the actual label of the batch encoded in $Y$. $Y$ has shape $n\\times10$ ($i$-th row = $i$-th batch element, $j$-th column = probablities for digit $j$) and its elements are\n",
    "<br><br>\\begin{align}\n",
    "y_{ij}=\\begin{cases}\n",
    "1 & \\text{$i$-th batch element has label j}\\\\\n",
    "0 & \\text{otherwise}.\n",
    "\\end{cases}\n",
    "\\end{align}<br>\n",
    "If the network output represents a probability distribution, one often uses the cross-entropy loss function. In our case the latter can be written as\n",
    "<br><br>\\begin{align}\n",
    "{\\cal{L}}=\\sum_i\\log{\\left[(Y\\cdot Y_{pred})_{ii}\\right]}.\n",
    "\\end{align}<br>\n",
    "The gradient with respect to the weight matrices is calculated for you in the function ce_gradient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 219,
   "metadata": {},
   "outputs": [],
   "source": [
    "#function that transforms digit label {0,1,...,9} to one-hot-encoding matrices of shape n*10\n",
    "def one_hot_mat(y_train):\n",
    "    \n",
    "    #get batch_size\n",
    "    batch_size = y_train.shape[0]\n",
    "    \n",
    "    #initialize matrix with zeros\n",
    "    Y_train = np.zeros([#TODO])\n",
    "    \n",
    "    #fill matrix with ones\n",
    "    for k in range(batch_size):\n",
    "        Y_train[#TODO] = 1.\n",
    "        \n",
    "    return Y_train\n",
    "\n",
    "#define error function\n",
    "def cross_entropy(X_train, y_train, W_1, W_2):\n",
    "    \n",
    "    #get batch_size\n",
    "    batch_size = x_train.shape[0]\n",
    "    \n",
    "    #get prediction\n",
    "    Y_pred = predict(X_train, W_1, W_2)[-1]\n",
    "    \n",
    "    #get one_hot_encoded label matrix\n",
    "    Y_train = one_hot_mat(y_train)\n",
    "\n",
    "    #calculate error\n",
    "    err = np.sum(np.log(np.diagonal(np.matmul(Y_train, Y_pred))))/batch_size\n",
    "    \n",
    "    return err\n",
    "\n",
    "#define gradient function\n",
    "def ce_gradient(y, y_pred, x_1, x_0, W_1, W_2, l_2_reg=0.5):\n",
    "    \n",
    "    #get batch_size\n",
    "    batch_size = y_pred.shape[1]\n",
    "    \n",
    "    #cross entropy loss gradient for W_1\n",
    "    grad_W_1 = -(np.matmul(x_0, (x_1.T*(1.-x_1).T*(W_2[y]-np.matmul(y_pred.T, W_2)))[:,1:]).T)/batch_size\n",
    "    \n",
    "    #l2-regularization loss for W_1\n",
    "    grad_W_1 += l_2_reg * W_1\n",
    "    \n",
    "    #cross entropy loss gradient for W_2\n",
    "    grad_W_2 = -(np.matmul(x_1, (one_hot_mat(y)-y_pred.T)).T)/batch_size\n",
    "    \n",
    "    #l2-regularization loss for W_2\n",
    "    grad_W_2 += l_2_reg * W_2\n",
    "    \n",
    "    return grad_W_1, grad_W_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Definition of training function (Adam optimizer)**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Adam optimizer is a state-of-the-art optimization method for neural networks. It is gradient-based, includes a momentum term to avoid local minima and adapts the step sizes via the estimation of the gradient's second moment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 198,
   "metadata": {},
   "outputs": [],
   "source": [
    "#define function that returns the updated weights\n",
    "def adam_opt(X_train, y_train, W_2, W_1, rate = 0.001, iterations=1000, batch_size=16, l_2_reg=0.5, decay_rate_s=0.9, decay_rate_r=0.999, delta = 10**(-8)):\n",
    "        \n",
    "    s_1 = np.zeros(W_1.shape)\n",
    "    r_1 = np.zeros(W_1.shape)\n",
    "    s_2 = np.zeros(W_2.shape)\n",
    "    r_2 = np.zeros(W_2.shape)\n",
    "    \n",
    "    acc_list = []\n",
    "    for i in range(iterations):\n",
    "        \n",
    "        #get random training sample\n",
    "        rand = np.random.randint(0, len(x_train), batch_size)\n",
    "        x = X_train[rand]\n",
    "        y = y_train[rand]\n",
    "        \n",
    "        #prediction of network\n",
    "        x_0, x_1, y_pred = predict(x, W_1, W_2)\n",
    "        #calculate gradients of cross entropy loss\n",
    "        grad_W_1, grad_W_2 = #TODO\n",
    "        \n",
    "        #first moment estimation for W_1\n",
    "        s_1 *= decay_rate_s\n",
    "        s_1 += (1-decay_rate_s) * grad_W_1\n",
    "        #second moment estimation for W_1\n",
    "        r_1 *= decay_rate_r\n",
    "        r_1 += (1-decay_rate_r) * np.square(grad_W_1)\n",
    "        #first moment estimation for W_2\n",
    "        s_2 *= decay_rate_s\n",
    "        s_2 += (1-decay_rate_s) * grad_W_2\n",
    "        #second moment estimation for W_2\n",
    "        r_2 *= decay_rate_r\n",
    "        r_2 += (1-decay_rate_r) * np.square(grad_W_2)\n",
    "        \n",
    "        #bias correction\n",
    "        s_1_dag = s_1/(1-decay_rate_s**(i+1))\n",
    "        r_1_dag = r_1/(1-decay_rate_r**(i+1))\n",
    "        s_2_dag = s_2/(1-decay_rate_s**(i+1))\n",
    "        r_2_dag = r_2/(1-decay_rate_r**(i+1))\n",
    "\n",
    "        #calculate updated first layer weight matrix\n",
    "        W_1_new = W_1 - rate * s_1_dag/(np.sqrt(r_1_dag) + delta)\n",
    "        \n",
    "        #calculate updated second layer weight matrix\n",
    "        W_2_new = W_2 - rate * s_2_dag/(np.sqrt(r_2_dag) + delta)\n",
    "        \n",
    "        #keep track of accuracy (comment out if it execution takes too long)\n",
    "        if(i%100 == 0):\n",
    "            acc = 0\n",
    "            output = np.argmax(predict(x_train, W_1, W_2)[-1], axis=0)\n",
    "            for k in range(len(x_train)):\n",
    "                if(output[k] == y_train[k]):\n",
    "                    acc = acc + 1\n",
    "            acc = acc/len(x_train)\n",
    "            acc_list.append([i,acc])\n",
    "        \n",
    "        #update weight matrices\n",
    "        W_1, W_2 = W_1_new, W_2_new\n",
    "    \n",
    "    return W_1_new, W_2_new, acc_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Execute training**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the execution takes too long try to comment out the lines under \"#keep track of accuracy\" in the function adam_opt. In the last line of the following cell one can adjust some parameters of the training process."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 224,
   "metadata": {},
   "outputs": [],
   "source": [
    "#set random seed for reproducible results\n",
    "np.random.seed(1)\n",
    "\n",
    "#initialize weights\n",
    "W_1, W_2 = #TODO\n",
    "\n",
    "#execution of training via adam optimizer\n",
    "W_1, W_2, acc_list = adam_opt(x_train, y_train, W_2, W_1, rate = 0.001, iterations=1000, batch_size=16, l_2_reg=0.00002, decay_rate_s=0.9, decay_rate_r=0.999)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Validation via accuracy**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "#calculate accuracy on training data\n",
    "acc = 0\n",
    "output = np.argmax(predict(x_train, W_1, W_2)[-1], axis=0)\n",
    "for i in range(len(x_train)):\n",
    "    if(output[i] == y_train[i]):\n",
    "        acc = acc + 1\n",
    "acc = #TODO\n",
    "print(\"training accuracy: \" + str(round(acc,1)) + \" %\")\n",
    "\n",
    "\n",
    "#calculate accuracy on test data\n",
    "acc = 0\n",
    "output = np.argmax(predict(x_test, W_1, W_2)[-1], axis=0)\n",
    "for i in range(len(x_test)):\n",
    "    if(output[i] == y_test[i]):\n",
    "        acc = acc + 1\n",
    "acc = #TODO\n",
    "print(\"test accuracy: \" + str(round(acc,1)) + \" %\")\n",
    "\n",
    "\n",
    "xx=np.array(acc_list)[:,0]\n",
    "yy=np.array(acc_list)[:,1]\n",
    "plt.xlabel('iteration')\n",
    "plt.ylabel('training accuracy')\n",
    "plt.plot(xx,yy)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Visualization of result for a random image**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look at the results for different randomly chosen images from the test set. For this just repeatedly execute the following cell. Are there numbers that are misclassified by the network more often?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#get random image\n",
    "rand = [np.random.randint(0, len(x_test))]\n",
    "\n",
    "#get prediction for random image from x_test\n",
    "x_0, x_1, x_2 = #TODO\n",
    "\n",
    "#print and plot prediction\n",
    "print(\"prediction for the random image: \")\n",
    "print(np.round(x_2,2))\n",
    "xx=range(0,10)\n",
    "yy=x_2[:,0]\n",
    "plt.xscale('linear')\n",
    "plt.yscale('linear')\n",
    "plt.bar(xx,yy)\n",
    "plt.show()\n",
    "\n",
    "#print label, plot image\n",
    "plt.matshow(x_test[rand[0]])\n",
    "print(\"label of the random image: \" + str(*y_test[rand]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Play with parameters**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The training of the neural network already works ok for the chosen parameters. However, the parameters are not optimal. Just change the parameters (learning rate, batch size, iterations, size of hidden layer, size of input image(e.g., set reduce_fac to 1), ...) in the above code and observe the change of training performance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Gradient-based optimization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the second part we want to look at the different behaviour of gradient-based optimizers. To this end we consider a one-dimensional objective function which should be minimized. Note that the gradient is calculated via an exact analytical formula for the derivative. This is different from above where it is only approximated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Necessary imports**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 165,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "%matplotlib inline\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "from ipywidgets import interact, interactive, fixed, interact_manual, IntSlider, IntText, Play, jslink"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Definition of objective function**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we define and plot the objective function. It is designed to have many local minima."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#parameters of oscillating error function\n",
    "omega = 5\n",
    "amplitude = 4\n",
    "#set plot resolution\n",
    "resolution=0.01\n",
    "\n",
    "#oscillating error\n",
    "def error_osci(x):\n",
    "    y = x**2 + amplitude*np.sin(omega*x)\n",
    "    return y\n",
    "#according gradient\n",
    "def error_osci_grad(x):\n",
    "    y = #TODO\n",
    "    return y\n",
    "\n",
    "#plot objective function\n",
    "fig, ax = plt.subplots()\n",
    "x = np.arange(-8, 8, 0.01)\n",
    "y = error_osci(x)\n",
    "ax.plot(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Definition of different optimizers**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 167,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gradient_descent(error, error_grad, x_init, step_size=0.001, iterations=100):\n",
    "    x_min = x_init\n",
    "    history = [[x_init,error(x_init)]]\n",
    "    for i in range(iterations):\n",
    "        grad = error_grad(x_min)\n",
    "        x_min -= step_size * grad\n",
    "        history.append([x_min, error(x_min)])\n",
    "    return history\n",
    "\n",
    "def ada_grad(error, error_grad, x_init, step_size=0.001, iterations=100, delta=10**(-7)):\n",
    "    x_min = x_init\n",
    "    history = [[x_init,error(x_init)]]\n",
    "    r = 0\n",
    "    for i in range(iterations):\n",
    "        grad = error_grad(x_min)\n",
    "        r += np.square(grad)\n",
    "        x_min -= step_size/(np.sqrt(r)+delta) * grad\n",
    "        history.append([x_min, error(x_min)])\n",
    "    return history\n",
    "\n",
    "def RMS_prop(error, error_grad, x_init, step_size=0.001, decay_rate=0.8, iterations=100, delta=10**(-6)):\n",
    "    x_min = x_init\n",
    "    history = [[x_init,error(x_init)]]\n",
    "    r = 0\n",
    "    for i in range(iterations):\n",
    "        grad = error_grad(x_min)\n",
    "        r *= decay_rate\n",
    "        r += (1-decay_rate) * np.square(grad)\n",
    "        x_min -= step_size/np.sqrt(r+delta) * grad\n",
    "        history.append([x_min, error(x_min)])\n",
    "    return history\n",
    "\n",
    "def adam(error, error_grad, x_init, step_size=0.001, decay_rate_1=0.9, decay_rate_2=0.999, iterations=100, delta=10**(-8)):\n",
    "    x_min = x_init\n",
    "    history = [[x_init,error(x_init)]]\n",
    "    s = 0\n",
    "    r = 0\n",
    "    for i in range(iterations):\n",
    "        grad = error_grad(x_min)\n",
    "        #first moment estimation\n",
    "        s *= decay_rate_1\n",
    "        s += (1-decay_rate_1) * grad\n",
    "        #second moment estimation\n",
    "        r *= decay_rate_2\n",
    "        r += (1-decay_rate_2) * np.square(grad)\n",
    "        #bias correction\n",
    "        s_dag = s/(1-decay_rate_1**(i+1))\n",
    "        r_dag = r/(1-decay_rate_2**(i+1))\n",
    "        x_min -= step_size*s_dag/(np.sqrt(r_dag)+delta)\n",
    "        history.append([x_min, error(x_min)])\n",
    "    return history"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Definition of plotting function**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 168,
   "metadata": {},
   "outputs": [],
   "source": [
    "left=-9\n",
    "right=7\n",
    "def plot_history(i, error, history):\n",
    "    fig, ax = plt.subplots()\n",
    "    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']\n",
    "    optimizer = list(history.keys())\n",
    "    #error function\n",
    "    x = np.arange(left, right, resolution)\n",
    "    y = error(x)\n",
    "    ax.plot(x, y)\n",
    "    #iteration step\n",
    "    for k in range(len(optimizer)):\n",
    "        if(i==0):\n",
    "            ax.plot(*np.array(history[optimizer[k]][0]).T.tolist(), color=colors[k], marker='o')\n",
    "        else:\n",
    "            ax.plot(*np.array(history[optimizer[k]][:i+1]).T.tolist(), color=colors[k], marker='o')\n",
    "    ax.grid()\n",
    "    plt.legend((\"error\",)+tuple(optimizer), loc='center left', bbox_to_anchor=(1, 0.5))\n",
    "    plt.show()\n",
    "    return"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Plotting**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe the optimization process of the different methods, especially plain gradient descent and Adam. Play with the learning rates (step_size_...) of the different optimizers and see how the behaviour changes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 169,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "9668ce647bf6430aa6406f72bdd34944",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "IntText(value=0)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "b5311794c8fb4ebb8e31d9994a46e5ee",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(Play(value=0, description='step', max=49), Output()), _dom_classes=('widget-interact',))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "#initial point\n",
    "x_init = -6.2\n",
    "\n",
    "#number of iteration steps\n",
    "iterations = 50\n",
    "\n",
    "#step size or learning rate of gradient-based algorithms\n",
    "step_size_gd = 0.02\n",
    "step_size_ag = 1.2\n",
    "step_size_RMS = 0.5\n",
    "step_size_adam = 1\n",
    "#decay rate of past squared gradients for RMS_prop\n",
    "decay_rate = 0.9\n",
    "#decay rate of past gradients for adam\n",
    "decay_rate_1 = 0.9\n",
    "#decay rate of past squared gradients for adam\n",
    "decay_rate_2 = 0.999\n",
    "\n",
    "#apply gradient-based methods and save history\n",
    "history = {\n",
    "            \"gradient_descent\": gradient_descent(error_osci, error_osci_grad, x_init, step_size_gd, iterations)\n",
    "           #,\"ada_grad\": ada_grad(error_osci, error_osci_grad, x_init, step_size_ag, iterations)\n",
    "           #,\"RMS_prop\": RMS_prop(error_osci, error_osci_grad, x_init, step_size_RMS, decay_rate, iterations)\n",
    "           ,\"adam\": adam(error_osci, error_osci_grad, x_init, step_size_adam, decay_rate_1, decay_rate_2, iterations)\n",
    "          }\n",
    "\n",
    "#plot\n",
    "play_widget = Play(min=0, max=iterations-1, step=1, value=0, description=\"step\")\n",
    "text_widget = IntText()\n",
    "display(text_widget)\n",
    "mylink = jslink((play_widget, 'value'), (text_widget, 'value'))\n",
    "interact(plot_history, i=play_widget, error=fixed(error_osci), history=fixed(history));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
