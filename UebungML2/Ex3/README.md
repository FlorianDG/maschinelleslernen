# Machine Learning 2 - Spiking Network Exercises

There are two different spiking networks exercises which rely on two different simulators, both based on python.
The first one is based on **PyNN+NEST**, a dedicated spiking network simulator.
The second one is based on **pyTorch**, a generic computation framework (like Tensorflow) where the neural equations are implemented.
This will give you a flavor of how learning is implemented in both cases.

## PyNN+NEST

Open the [provided jupyter notebook](pynn/ml2-pynn-exercise.ipynb), it will guide you for the installation.
The exercise consists of learning of visual features with spiking neural networks and spike-time-dependent-plasticity (STDP).
In this case, we take a bottom-up approach and try to learn features with plasticity rules observed by neuroscientists in an unsupervised fashion.
Do not spend too much time and energy trying to learn good features, jump to the SpyTorch tutorial instead.

## SpyTorch

This is not an exercise, but a tutorial on learning with spikes pytorch, authored by [Friederman Zenke](https://fzenke.net/).
In this case, we take a top-down approach and adapt deep learning techniques to biologically plausible neural dynamics.
See the official [pytorch website](https://pytorch.org/) for installation instructions (GPU is not necessary).
The folder contains three ordered notebook tutorials:
1. [Neural dynamics and learning](spytorch/notebooks/SpyTorchTutorial1.ipynb)
2. [Learning on a real vision dataset](spytorch/notebooks/SpyTorchTutorial2.ipynb)
3. [Regularizing for sparsity](spytorch/notebooks/SpyTorchTutorial3.ipynb)
